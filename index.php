<!-- 
Создайте "базу" городов. Это, конечно же, массив с названиями городов, который вам придется заполнить вручную в коде.
Напишите функцию (и тест для нее), которая по первой букве названия города вёрнет из этого массива первое встреченное название города на эту букву.
Напишите скрипт, который примет из формы введенное пользователем название города, определит у него последнюю букву и, применив функцию из пункта 2, "сделает ход" - напечатает название города, который начинается на эту букву
-->

<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<title>Mod1_less5</title>
</head>
<body>
	<form action="/step.php" method="post">
		<input type="text" name="city">
		<button send="send">Send</button>
	</form>
</body>
</html>