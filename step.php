<?php
require __DIR__ . '/lib/functions.php';

/* Напишите скрипт, который примет из формы введенное пользователем название города, определит у него последнюю букву и, применив функцию из пункта 2, "сделает ход" - напечатает название города, который начинается на эту букву */

// получаем массив с городами
$city = include(__DIR__ . '/lib/cityDb.php');

// получаем последний символ из элемента массива $_POST
$lastCityLetter = mb_substr($_POST['city'], -1, 1, 'UTF-8');

// вызываем функцию поиска и записываем результат в переменную для вывода.
$result = findCity($lastCityLetter, $city); 

?>

<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<title>City</title>
</head>
<body>
	<?php if (!is_null($result)) { ?>

	<p><?php echo $_POST['city']; ?> -> <b><?php echo $result; ?></b></p>
	<p><a href="/">back</a></p>

	<?php } else { ?>

	<p>Something went wrong</p>
	<p><a href="/">back</a></p>

	<?php } ?>
	
</body>
</html>